# FROM busybox:latest
# RUN mkdir build
# RUN cd build
# RUN touch house.txt
# RUN echo "walls" >> house.txt
# RUN echo "floor" >> house.txt
FROM busybox:latest

RUN mkdir build &&\
    cd build &&\
    touch house.txt &&\
    echo "walls" >> house.txt \
    echo "floor" >> house.txt
